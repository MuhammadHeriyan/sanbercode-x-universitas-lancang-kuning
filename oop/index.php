<?php
    require_once("Animal.php");
    require_once("Frog.php");
    require_once("Ape.php");

    $animal = new Animal("shaun");

    echo "Name : " . $animal->name . "<br>";
    echo "legs : " . $animal->legs . "<br>";
    echo "cold blooded : " . $animal->cold_blooded . "<br> <br>";

    $frog = new Frog("buduk");

    echo "Name : " . $frog->name . "<br>";
    echo "legs : " . $frog->legs . "<br>";
    echo "cold blooded : " . $frog->cold_blooded . "<br>";
    echo "Jump : " . $frog->jump() . "<br> <br>";

    $ape = new Ape("kera sakti");

    echo "Name : " . $ape->name . "<br>";
    echo "legs : " . $ape->legs . "<br>";
    echo "cold blooded : " . $ape->cold_blooded . "<br>";
    echo "Yell : " . $ape->yell() . "<br>";
?>