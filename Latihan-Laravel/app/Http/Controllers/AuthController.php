<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('Register');
    }

    public function welcome(Request $requet)
    {
        $fname = $requet->input('firstname');
        $lname = $requet->input('lastname');

        return view('page.Welcome', ['fname' => $fname, 'lname' => $lname]);
    }
}
