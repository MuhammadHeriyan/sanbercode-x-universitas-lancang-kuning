@extends('layouts.master')
@section('title')
Halaman Register
@endsection
@section('content')
    <h1>Buat Account Baru!</h1><br />
    <h2>Sign Up Form</h2><br />
    
    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label> <br />
        <input type="text" name="firstname" /> <br />
        <br />
        <label>Last name:</label> <br />
        <input type="text" name="lastname" /> <br />
        <br />
        <label>Gender:</label> <br />
        <input type="radio" value="1" name="jenis_gender" /> Man <br />
        <input type="radio" value="2" name="jenis_gender" /> Woman <br />
        <input type="radio" value="3" name="jenis_gender" /> Other <br />
        <br />
        <label>Nationality:</label>
        <select name="negara">
            <option value="1">Indonesia</option>
            <option value="2">Singapura</option>
            <option value="3">Malaysia</option>
            <option value="4">Thailand</option>
        </select> <br />
        <br />
        <label>Language Spoken:</label> <br />
        <input type="checkbox" value="1" name="bahasa" /> Bahasa Indonesia <br />
        <input type="checkbox" value="1" name="bahasa" /> English <br />
        <input type="checkbox" value="1" name="bahasa" /> Arabic <br />
        <input type="checkbox" value="1" name="bahasa" /> Japanese <br />
        <br />
        <label>Bio:</label> <br />
        <textarea name="bio_area" cols="30" rows="10"></textarea> <br />
        <br />
        <input type="submit" value="Sign Up">
    </form>
    <a href="/">Kembali</a>
@endsection