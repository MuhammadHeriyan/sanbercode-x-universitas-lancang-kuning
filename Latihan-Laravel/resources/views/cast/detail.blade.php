@extends('layouts.master')

@section('title')
Halaman Detail Pemain Film
@endsection

@section('content')

<h1>{{$cast1->nama}}</h1>
<p>{{$cast1->umur}}</p>
<p>{{$cast1->bio}}</p>

<a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>

@endsection